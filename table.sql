CREATE TABLE garage_readings (
	collect_date timestamptz(6) NOT NULL,
	location VARCHAR(280) NOT NULL,
	pm_10 NUMERIC(16) NOT NULL,
	pm_25 NUMERIC(16) NOT NULL,
	pm_100 NUMERIC(16) NOT NULL,
	pm_10_env NUMERIC(16) NOT NULL,
	pm_25_env NUMERIC(16) NOT NULL,
	pm_100_env NUMERIC(16) NOT NULL,
	particles_03um NUMERIC(16) NOT NULL,
	particles_05um NUMERIC(16) NOT NULL,
	particles_10um NUMERIC(16) NOT NULL,
	particles_25um NUMERIC(16) NOT NULL,
	particles_50um NUMERIC(16) NOT NULL,
	particles_100um NUMERIC(16) NOT NULL,
	CONSTRAINT collection_date_pkey PRIMARY KEY (collect_date, location)
)

collect_data, location, pm_10, pm_25, pm_100, pm_10_env, pm_25_env, pm_100_env, particles_03um, particles_05um, particles_10um, particles_25um, particles_50um, particles_100um
