from flask import Flask, jsonify
import psycopg2
from psycopg2.extras import DictCursor
import json
from configparser import ConfigParser
app = Flask(__name__)

# @TODO: move to common area, copied from pm5003.py
def config(filename='database.ini', section='postgresql'):
    
    parser = ConfigParser()
    parser.read(filename)
    
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
    return db

# connection
conn = None
app.config['database'] = psycopg2.connect(**config(filename='../database.ini'))

@app.route('/')
def welcome():
    return "Welcome"

@app.route('/locations')
def get_locations():
    cur = app.config['database'].cursor()
    try:
        sql = "SELECT DISTINCT location FROM garage_readings"
        cur.execute(sql)
        records = [row[0] for row in cur.fetchall()]
    finally:
        cur.close()
        
    return jsonify(locations=records), 200


@app.route("/total_records/<location>")
def get_total_records(location : str):
    cur = app.config['database'].cursor()
    try:
        sql = "SELECT count(*) AS total_records from garage_readings WHERE location = %s"
        cur.execute(sql, (location,))
        res = cur.fetchone()
    finally:
        cur.close()
    
    if res:
        return jsonify(total_records=res[0]), 200
    else:
        return jsonify(msg=f"{location} not found"), 404

    
@app.route("/records/<location>/hours/<hours>")
def get_today(location : str, hours : int):
    if int(hours) > 8:
        return jsonify(msg="NOPE"), 400
    cur = app.config['database'].cursor(cursor_factory=DictCursor)
    try:
        sql = "SELECT * from garage_readings WHERE location = %s AND collect_date BETWEEN NOW() - INTERVAL '8 HOURS' AND NOW()" # @TODO: LIMIT TO today!
        cur.execute(sql, (location,))
        result = [row for row in cur.fetchall()]
    finally:
        cur.close()
    
    if result:
        return jsonify(result=result), 200
    else:
        return jsonify(msg=f"{location} not found"), 404
    
